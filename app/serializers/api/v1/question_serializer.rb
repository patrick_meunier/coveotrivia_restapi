# == Schema Information
#
# Table name: api_v1_questions
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Api::V1::QuestionSerializer < ActiveModel::Serializer
  attributes :id, :title, :body, :user_id

  belongs_to :user
end
