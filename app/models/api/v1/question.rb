# == Schema Information
#
# Table name: api_v1_questions
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Api::V1::Question < ApplicationRecord
  belongs_to :user
end
