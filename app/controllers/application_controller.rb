# Application wide logic
class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
end
