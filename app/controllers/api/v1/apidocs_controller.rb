# class Api::V1::ApidocsController < ApplicationController
#   include Swagger::Blocks
#
#   swagger_root do
#     key :swagger, '2.0'
#     info do
#       key :version, '1.0.0'
#       key :title, 'Swagger Coveo Trivia'
#       key :description, 'Coveo Trivia Rest documentation'
#     end
#     contact do
#       key :name, 'Patrick Meunier'
#     end
#     license do
#       key :name, 'Coveo'
#     end
#     key :host, 'localhost:3000'
#     key :basePath, '/api/v1'
#     key :consumes, ['application/json']
#     key :produces, ['application/json']
#   end
#     # A list of all classes that have swagger_* declarations.
#     SWAGGERED_CLASSES = [
#       UsersController,
#       User,
#       self,
#     ].freeze
#
#     def index
#       render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
#     end
# end
