class Api::V1::ApiController < ApplicationController
  include Swagger::Blocks
  respond_to :json
end
