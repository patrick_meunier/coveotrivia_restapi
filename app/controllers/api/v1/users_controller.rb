class Api::V1::UsersController < Api::V1::ApiController
  respond_to :json
  # GET /v1/users
  def index
    @users = User.all
    render json: @users
  end

  # GET /v1/users/{id}
  def show
    render json: User.find(params[:id])
  end
end
