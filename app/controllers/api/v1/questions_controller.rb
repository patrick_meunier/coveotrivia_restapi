class Api::V1::QuestionsController < ApplicationController
  before_action :set_v1_questions, only: [:show, :update, :destroy]

  # GET /api/v1/questions
  def index
    @v1_questionss = Api::V1::Question.all

    render json: @v1_questionss
  end

  # GET /api/v1/questions/1
  def show
    render json: @v1_questions
  end

  # POST /api/v1/questions
  def create
    @v1_questions = Api::V1::Question.new(v1_question_params)

    if @v1_questions.save
      render json: @v1_questions, status: :created, location: @v1_questions
    else
      render json: @v1_questions.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/questions/1
  def update
    if @v1_questions.update(v1_question_params)
      render json: @v1_questions
    else
      render json: @v1_questions.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/questions/1
  def destroy
    @v1_questions.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_v1_questions
      @v1_questions = Api::V1::Question.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def v1_question_params
      params.require(:question).permit(:id,:title, :body, :user_id, :created_at, :updated_at)
    end
end
