OmniAuth.config.logger = Rails.logger
# we fix Protocol Mismatch for redirect_uri in Rails
OmniAuth.config.full_host = Rails.env.production? ? 'https://domain.com' : 'http://localhost:3000'
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer unless Rails.env.production?
  provider :google_oauth2, ENV['GOOGLE_CLIENT_ID'], ENV['GOOGLE_CLIENT_SECRET'], {
            name: 'google',
            prompt: "select_account",
            image_aspect_ratio: "square",
            image_size: 50
            }
end
