class CreateApiV1Questions < ActiveRecord::Migration[5.0]
  def change
    create_table :api_v1_questions do |t|
      t.string :title
      t.text :body
      t.references :user
      t.timestamps
    end
    add_foreign_key :api_v1_questions, :users, column: :user_id
  end
end
