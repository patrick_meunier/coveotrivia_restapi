class CreateApiV1Answers < ActiveRecord::Migration[5.0]
  def change
    create_table :api_v1_answers do |t|
      t.string :title
      t.text :body
      t.references :user, :question
      t.timestamps
    end
  end
end
