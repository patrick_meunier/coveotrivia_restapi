require 'test_helper'

class Api::V1::AnswersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @api_v1_answer = api_v1_answers(:one)
  end

  test "should get index" do
    get api_v1_answers_url, as: :json
    assert_response :success
  end

  test "should create api_v1_answer" do
    assert_difference('Api::V1::Answer.count') do
      post api_v1_answers_url, params: { api_v1_answer: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show api_v1_answer" do
    get api_v1_answer_url(@api_v1_answer), as: :json
    assert_response :success
  end

  test "should update api_v1_answer" do
    patch api_v1_answer_url(@api_v1_answer), params: { api_v1_answer: {  } }, as: :json
    assert_response 200
  end

  test "should destroy api_v1_answer" do
    assert_difference('Api::V1::Answer.count', -1) do
      delete api_v1_answer_url(@api_v1_answer), as: :json
    end

    assert_response 204
  end
end
