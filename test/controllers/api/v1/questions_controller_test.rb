require 'test_helper'

class Api::V1::QuestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    DatabaseCleaner.start
    @v1_question = build_stubbed(:question)
    @v1_question_saved = create(:question)
    @question_attr = attributes_for(:question)
  end

  teardown do
    DatabaseCleaner.clean
  end

  test "should get index" do
    get v1_questions_url, as: :json
    assert_response :success
  end

  test "should successfully create a question" do
    assert_difference('Api::V1::Question.count') do
      post v1_questions_url, params: {question: @v1_question}, as: :json
      put response.body
    end
    assert_response :success
    assert_equal({id: Api::V1::Question.last.id, title: "patrick" }, response.parsed_body)
  end

  test "should show v1_questions" do
    get v1_question_url(@v1_question_saved), as: :json
    assert_response :success
  end

  test "should update v1_question" do
    patch v1_questions_url(@v1_question_saved.id), params: {question: @question_attr}, as: :json
    assert_response 200
  end

  test "should destroy v1_questions" do
    assert_difference('Api::V1::Question.count', -1) do
      delete v1_question_url(@v1_question_saved), as: :json
    end

    assert_response 204
  end
end
