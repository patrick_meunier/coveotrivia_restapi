ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/rails"
# output
require "minitest/reporters"
# test coverage tool
require 'simplecov'
# Runs all tests before Ruby exits, using `Kernel#at_exit`.
require 'minitest/autorun'
# Enables rainbow-coloured test output.
require 'minitest/pride'
# Enables parallel (multithreaded) execution for all tests.
require 'minitest/hell'

SimpleCov.start 'rails' unless ENV['NO_COVERAGE']
Minitest::Reporters.use! [Minitest::Reporters::ProgressReporter.new(color: true), Minitest::Reporters::MeanTimeReporter.new]

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  # fixtures :all
  # Add more helper methods to be used by all tests here...
  include FactoryGirl::Syntax::Methods
  require 'faker'
  require 'database_cleaner'
  DatabaseCleaner.clean_with :truncation
  DatabaseCleaner.strategy = :truncation
end
