# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string           default("email"), not null
#  uid                    :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  name                   :string
#  nickname               :string
#  image                  :string
#  email                  :string
#  tokens                 :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

FactoryGirl.define do
  sequence(:random_id) {|n| @random_ids ||= (1..100000).to_a.shuffle; @random_ids[n] }

  factory :user do
    # id { FactoryGirl.generate(:random_id) }
    nickname Faker::GameOfThrones.character
    name Faker::Name.first_name + " " + Faker::Name.last_name
    email { Faker::Internet.safe_email(name) }
    password 12345678
    provider "google"
    uid Faker::IDNumber.valid
  end
end
