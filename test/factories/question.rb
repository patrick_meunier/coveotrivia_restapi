# == Schema Information
#
# Table name: api_v1_questions
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryGirl.define do
  factory :question, class: Api::V1::Question do
    title Faker::Book.title
    body Faker::Lorem.paragraphs 1
    user
  end
end
