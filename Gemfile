source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Secret environment variables. Put first if other gems needs variable before initialization
gem 'dotenv-rails', groups: [:development, :test]
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
# json formatter
gem 'active_model_serializers'
# user postgresql for active record database
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# swagger API documentation
gem 'swagger-blocks'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
# Devise authentication system
gem 'devise'
# devise token authentication
gem 'devise_token_auth'
# omniauth librairy
gem 'omniauth'
# google omniauth2 librairie
gem 'omniauth-google-oauth2', '~> 0.2.1'
# Use Capistrano for deployment
gem 'capistrano-rails', group: :development
# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS),
# making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'
# file upload
gem 'carrierwave', '~> 1.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'pry-stack_explorer'
  # For managing testing data
  gem 'factory_girl_rails'
  gem 'faker'
end

group :development do
  # gem 'brakeman', require: 'false'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background.
  # Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'guard'
  gem 'guard-minitest'
  gem 'mailcatcher'
  gem "bullet"
end

group :test do
  gem 'minitest'
  gem 'minitest-rails'
  # For better display
  gem 'minitest-reporters'
  gem 'codeclimate-test-reporter', require: nil
  gem 'simplecov', require: false
  gem 'database_cleaner'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
